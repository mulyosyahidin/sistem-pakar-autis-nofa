<?php

use App\Models\Jenis_autis;
use App\Models\Kriteria;
use Illuminate\Support\Facades\Cache;

if (!function_exists('activeClass')) {
    /**
     * Membuat kelas `active` di link sidebar
     * 
     * Membuat kelas `active` pada item sidebar yang sedang dibuka berdasarkan
     * parameter `$route`. Jika URL saat ini match dengan `$route`, maka
     * akan mengembalikan `active`.
     * 
     * @param array $routes routes yang akan diperiksa
     * @param string $class nama kelas yang dihasilkan
     * @param array $queries    query string yang akan diperiksa
     * @return string|null
     */
    function activeClass($routes = [], $class = 'active', $queries = [])
    {
        if (is_string($routes)) {
            $routes = [$routes];
        }

        foreach ($routes as $key => $value) {
            if (request()->routeIs($value)) {
                // If current route is equal to given route, return active class
                return $class;
            }
        }

        if (count($queries) > 0) {
            foreach ($queries as $key => $value) {
                if (request()->query($key) == $value) {
                    return $class;
                }
            }
        }
    }
}


if (!function_exists('numberFormat')) {
    /**
     * Format number
     * 
     * Format number with given decimal points, decimal point character, and thousands separator
     * 
     * @param   $number         Number to format
     * @param   $decimals       Number of decimal points
     * @param   $decPoint       Decimal point character
     * @param   $thousandsSep   Thousands separator
     * @return  string
     */
    function numberFormat($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $negation = ($number < 0) ? (-1) : 1;
        $coefficient = 10 ** $decimals;
        $number = $negation * floor((string)(abs($number) * $coefficient)) / $coefficient;

        $result = number_format($number, $decimals, $decPoint, $thousandsSep);

        //get number behind comma
        $numberBehindComma = str_replace($decPoint, '', substr($result, strpos($result, $decPoint) + 1));

        // if number behind comma is 0, remove comma
        if ($numberBehindComma == 0) {
            $result = substr($result, 0, strpos($result, $decPoint));
        }

        return $result;
    }
}

if (!function_exists('dataKriteria')) {
    /**
     * Mengambil data kriteria
     * 
     * Mengambil data kriteria dari database. Jika data sudah ada di cache,
     * maka akan mengambil data dari cache. Jika tidak, maka akan mengambil
     * data dari database dan menyimpannya di cache.
     * 
     * @param int $id id kriteria
     * @return \App\Models\Kriteria
     */
    function dataKriteria($id)
    {
        $key = 'kriteria_' . $id;

        if (Cache::has($key))
            return Cache::get($key);

        $kriteria = Kriteria::find($id);

        Cache::put($key, $kriteria, 86400);

        return $kriteria;
    }
}

if (!function_exists('hasilNilai')) {
    /**
     * Mengambil hasil nilai tertinggi
     * 
     * Mengambil hasil nilai tertinggi dari data yang diberikan. Data yang
     * diberikan harus berupa array dengan key adalah id jenis autis dan
     * value adalah nilai.
     * 
     * @param array $data data yang akan diambil nilai tertingginya
     * @return array
     */
    function hasilNilai($data)
    {
        //get highest value and key
        $highest = max($data);

        //get key of highest value
        $key = array_search($highest, $data);

        $autis =  Jenis_autis::find($key);

        return [
            'autis' => $autis,
            'nilai' => $highest,
        ];
    }
}
