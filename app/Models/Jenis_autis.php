<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenis_autis extends Model
{
    use HasFactory;

    /**
     * Tabel yang digunakan model
     *
     * @var string
     */
    protected $table = 'tb_jenis_autis';

    /**
     * Tabel tidak memiliki kolom waktu (created_at dan updated_at)
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Kolom tabel
     *
     * @var array
     */
    protected $fillable = [
        'nama',
        'solusi',
    ];

    /**
     * Relasi Many to Many ke tabel Gejala
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function gejala()
    {
        return $this->belongsToMany(Gejala::class, 'tb_gejala_autis', 'id_jenis', 'id_gejala');
    }
}
