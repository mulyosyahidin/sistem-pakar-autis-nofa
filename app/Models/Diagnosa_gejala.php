<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Diagnosa_gejala extends Pivot
{
    use HasFactory;

    /**
     * Tabel yang digunakan model
     *
     * @var string
     */
    protected $table = 'tb_diagnosa_gejala';

    /**
     * Kolom tabel
     *
     * @var array
     */
    protected $fillable = [
        'id_diagnosa',
        'id_gejala',
        'id_kriteria',
    ];

    /**
     * Relasi many-to-one dengan model Diagnosa
     * 
     * Relasi ini mendefinisikan bahwa model Diagnosa_gejala
     * dimiliki oleh model Diagnosa. Diagnosa_gejala memiliki foreign key
     * id_diagnosa yang terhubung dengan field id pada model Diagnosa.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function diagnosa()
    {
        return $this->belongsTo(Diagnosa::class, 'id_diagnosa');
    }

    /**
     * Relasi many-to-one dengan model Gejala
     * 
     * Relasi ini mendefinisikan bahwa model Diagnosa_gejala
     * dimiliki oleh model Gejala. Diagnosa_gejala memiliki foreign key
     * id_gejala yang terhubung dengan field id pada model Gejala.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gejala()
    {
        return $this->belongsTo(Gejala::class, 'id_gejala');
    }

    /**
     * Relasi many-to-one dengan model Kriteria
     * 
     * Relasi ini mendefinisikan bahwa model Diagnosa_gejala
     * dimiliki oleh model Kriteria. Diagnosa_gejala memiliki foreign key
     * id_kriteria yang terhubung dengan field id pada model Kriteria.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kriteria()
    {
        return $this->belongsTo(Kriteria::class, 'id_kriteria');
    }
}
