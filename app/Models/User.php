<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, InteractsWithMedia, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * Get the profile picture of the user.
     * 
     * @return string
     */
    public function getProfilePicture()
    {
        if ($this->getFirstMedia('profile_picture')) {
            return $this->getFirstMedia('profile_picture')->getUrl();
        }

        return 'https://ui-avatars.com/api/?background=random&rounded=true&color=fff&name=' . $this->name;
    }

    /**
     * Data diagnosa yang dimiliki user.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function diagnosa()
    {
        return $this->hasMany(Diagnosa::class);
    }
}
