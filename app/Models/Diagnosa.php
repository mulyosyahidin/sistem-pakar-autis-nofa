<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diagnosa extends Model
{
    use HasFactory;

    /**
     * Tabel yang digunakan model
     *
     * @var string
     */
    protected $table = 'tb_diagnosa';

    /**
     * Kolom tabel
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'judul',
        'id_jenis_autis',
        'nilai',
    ];

    /**
     * Relasi many-to-one dengan tabel user
     * 
     * Relasi ini mendefinisikan bahwa model Diagnosa dimiliki oleh model User.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relasi many-to-one dengan tabel jenis_autis
     * 
     * Relasi ini mendefinisikan bahwa model Diagnosa dimiliki oleh model Jenis_autis.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jenisAutis()
    {
        return $this->belongsTo(Jenis_autis::class, 'id_jenis_autis');
    }

    /**
     * Relasi many-to-many dengan tabel gejala
     * 
     * Relasi ini mendefinisikan bahwa model Diagnosa memiliki banyak model Gejala.
     * Relasi ini mendefinisikan pivot table tb_diagnosa_gejala.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function gejala()
    {
        return $this->belongsToMany(Gejala::class, 'tb_diagnosa_gejala', 'id_diagnosa', 'id_gejala')
            ->withPivot('id_kriteria')
            ->using(Diagnosa_gejala::class);
    }
}
