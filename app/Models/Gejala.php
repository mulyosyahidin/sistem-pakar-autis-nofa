<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gejala extends Model
{
    use HasFactory;

    /**
     * Tabel yang digunakan model
     *
     * @var string
     */
    protected $table = 'tb_gejala';

    /**
     * Tabel tidak memiliki kolom waktu (created_at dan updated_at)
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Kolom tabel
     *
     * @var array
     */
    protected $fillable = [
        'kode',
        'gejala',
        'bobot',
    ];

    /**
     * Relasi Many to Many ke tabel Jenis_autis
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function jenisAutis()
    {
        return $this->belongsToMany(Jenis_autis::class, 'tb_gejala_autis', 'id_gejala', 'id_jenis');
    }
}
