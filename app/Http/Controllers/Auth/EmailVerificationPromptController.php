<?php

namespace App\Http\Controllers\Auth;

use App\Enums\UserRole;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Providers\RouteServiceProvider;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     */
    public function __invoke(Request $request): RedirectResponse|View
    {
        return $request->user()->hasVerifiedEmail()
            ? (($request->user()->role == UserRole::ADMIN->value) ? redirect()->intended(RouteServiceProvider::ADMIN_HOME) : redirect()->intended(RouteServiceProvider::USER_HOME))
            : view('auth.verify-email');
    }
}
