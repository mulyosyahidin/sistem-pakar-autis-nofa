<?php

namespace App\Http\Controllers\Auth;

use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\RedirectResponse;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

class VerifyEmailController extends Controller
{
    /**
     * Mark the authenticated user's email address as verified.
     */
    public function __invoke(EmailVerificationRequest $request): RedirectResponse
    {
        if ($request->user()->hasVerifiedEmail()) {
            if ($request->user()->role == UserRole::ADMIN->value) {
                return redirect()->intended(RouteServiceProvider::ADMIN_HOME . '?verified=1');
            } else {
                return redirect()->intended(RouteServiceProvider::USER_HOME . '?verified=1');
            }
        }

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        if ($request->user()->role == UserRole::ADMIN->value) {
            return redirect()->intended(RouteServiceProvider::ADMIN_HOME . '?verified=1');
        } else {
            return redirect()->intended(RouteServiceProvider::USER_HOME . '?verified=1');
        }
    }
}
