<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lokasi_pengobatan;
use Illuminate\Http\Request;

class LokasiPengobatanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Lokasi_pengobatan::orderBy('nama')->get();

        return view('admin.lokasi-pengobatan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.lokasi-pengobatan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:255|unique:tb_lokasi_pengobatan,nama',
            'alamat' => 'nullable|string',
            'keterangan' => 'nullable|string',
            'latitude' => 'nullable|string',
            'longitude' => 'nullable|string',
        ]);

        $lokasi_pengobatan = Lokasi_pengobatan::create($request->all());

        return redirect()
            ->route('admin.lokasi-pengobatan.show', $lokasi_pengobatan)
            ->withSuccess('Berhasil menambah lokasi pengobatan baru');
    }

    /**
     * Display the specified resource.
     */
    public function show(Lokasi_pengobatan $lokasi_pengobatan)
    {
        return view('admin.lokasi-pengobatan.show', compact('lokasi_pengobatan'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Lokasi_pengobatan $lokasi_pengobatan)
    {
        return view('admin.lokasi-pengobatan.edit', compact('lokasi_pengobatan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Lokasi_pengobatan $lokasi_pengobatan)
    {
        $request->validate([
            'nama' => 'required|string|max:255|unique:tb_lokasi_pengobatan,nama,' . $lokasi_pengobatan->id,
            'alamat' => 'nullable|string',
            'keterangan' => 'nullable|string',
            'latitude' => 'nullable|string',
            'longitude' => 'nullable|string',
        ]);

        $lokasi_pengobatan->update($request->all());

        return redirect()
            ->route('admin.lokasi-pengobatan.show', $lokasi_pengobatan)
            ->withSuccess('Berhasil memperbarui data lokasi pengobatan');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Lokasi_pengobatan $lokasi_pengobatan)
    {
        $lokasi_pengobatan->delete();

        return redirect()
            ->route('admin.lokasi-pengobatan.index')
            ->withSuccess('Berhasil menghapus data lokasi pengobatan');
    }
}
