<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Diagnosa;
use Illuminate\Http\Request;

class DiagnosaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Diagnosa::with('user', 'jenisAutis')->latest()->get();

        return view('admin.diagnosa.index', compact('data'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Diagnosa $diagnosa)
    {
        $diagnosa->load('user', 'jenisAutis', 'gejala');

        return view('admin.diagnosa.show', compact('diagnosa'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Diagnosa $diagnosa)
    {
        $diagnosa->delete();

        return redirect()
            ->route('admin.diagnosa.index')
            ->withSuccess('Berhasil menghapus data diagnosa');
    }
}
