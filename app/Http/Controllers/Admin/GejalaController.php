<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gejala;
use Illuminate\Http\Request;

class GejalaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Gejala::orderBy('kode')->get();

        return view('admin.gejala.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.gejala.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode' => 'required|string|max:4|unique:tb_gejala,kode',
            'gejala' => 'required',
            'bobot' => 'required|numeric',
        ]);

        Gejala::create($request->all());

        return redirect()
            ->route('admin.gejala.index')
            ->withSuccess('Berhasil menambah data gejala baru');
    }

    /**
     * Display the specified resource.
     */
    public function show(Gejala $gejala)
    {
        return view('admin.gejala.show', compact('gejala'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Gejala $gejala)
    {
        return view('admin.gejala.edit', compact('gejala'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Gejala $gejala)
    {
        $request->validate([
            'kode' => 'required|string|max:4|unique:tb_gejala,kode,' . $gejala->id,
            'gejala' => 'required',
            'bobot' => 'required|numeric',
        ]);

        $gejala->update($request->all());

        return redirect()
            ->route('admin.gejala.show', $gejala)
            ->withSuccess('Berhasil memperbarui data gejala');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Gejala $gejala)
    {
        $gejala->delete();

        return redirect()
            ->route('admin.gejala.index')
            ->withSuccess('Berhasil menghapus data gejala');
    }
}
