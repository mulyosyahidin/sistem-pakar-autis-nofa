<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kriteria;
use Illuminate\Http\Request;

class KriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Kriteria::all();

        return view('admin.kriteria.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.kriteria.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'keterangan' => 'required',
            'nilai' => 'required|numeric',
        ]);

        Kriteria::create($request->all());

        return redirect()
            ->route('admin.kriteria.index')
            ->withSuccess('Berhasil menambah data kriteria baru');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Kriteria $kriterium)
    {
        return view('admin.kriteria.edit', compact('kriterium'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Kriteria $kriterium)
    {
        $request->validate([
            'keterangan' => 'required',
            'nilai' => 'required|numeric',
        ]);

        $kriterium->update($request->all());

        return redirect()
            ->route('admin.kriteria.index')
            ->withSuccess('Berhasil memperbarui data kriteria');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Kriteria $kriterium)
    {
        $kriterium->delete();

        return redirect()
            ->route('admin.kriteria.index')
            ->withSuccess('Berhasil menghapus data kriteria');
    }
}
