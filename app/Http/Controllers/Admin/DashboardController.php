<?php

namespace App\Http\Controllers\Admin;

use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Models\Diagnosa;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $count = [
            'jenis_autis' => \App\Models\Jenis_autis::count(),
            'gejala' => \App\Models\Gejala::count(),
            'user' => \App\Models\User::whereRole(UserRole::USER->value)->count(),
            'diagnosa' => \App\Models\Diagnosa::count(),
        ];

        $diagnosa = Diagnosa::with('user', 'jenisAutis')->latest()->take(5)->get();

        return view('admin.dashboard', compact('count', 'diagnosa'));
    }
}
