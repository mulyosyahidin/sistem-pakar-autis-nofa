<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gejala;
use App\Models\Jenis_autis;
use App\Models\Kriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PerhitunganController extends Controller
{
    public function pilihGejala()
    {
        $gejala = Gejala::orderByRaw("CONVERT(SUBSTRING(kode, 2), SIGNED)")->get();
        $kriteria = Kriteria::orderBy('nilai')->get();

        return view('admin.perhitungan.pilih-gejala', compact('gejala', 'kriteria'));
    }

    public function perhitungan(Request $request)
    {
        $request->validate([
            'nilai' => 'required|array',
        ]);

        $nilai = $request->nilai;

        session()->put('nilai', $nilai);

        $jenisAutis = Jenis_autis::with('gejala')->orderBy('nama')->get();

        $dataNilai = [];

        foreach ($jenisAutis as $autis) {
            $n = 1;

            foreach ($autis->gejala as $gejala) {
                $dataNilai[$autis->id][$n] = [
                    'pakar' => $gejala->bobot,
                    'user' => dataKriteria($nilai[$gejala->id])->nilai,
                    'perkalian' => $gejala->bobot * dataKriteria($nilai[$gejala->id])->nilai,
                ];

                $n++;
            }
        }

        return view('admin.perhitungan.perhitungan', compact('jenisAutis', 'nilai', 'dataNilai'));
    }

    public function simpan(Request $request)
    {
        if (!session()->has('nilai')) {
            return redirect()
                ->route('admin.perhitungan.pilih-gejala')
                ->withError('Silahkan pilih gejala terlebih dahulu');
        }

        $dataNilai = session()->get('nilai');

        $diagnosa = Auth::user()->diagnosa()->create([
            'judul' => 'Diagnosa ' . date('d-m-Y H:i:s'),
            'id_jenis_autis' => $request->id_jenis_autis,
            'nilai' => $request->nilai,
        ]);

        foreach ($dataNilai as $idGejala => $idKriteria) {
            $diagnosa->gejala()->attach($idGejala, [
                'id_kriteria' => $idKriteria,
            ]);
        }

        session()->forget('nilai');

        return redirect()
            ->route('admin.diagnosa.show', $diagnosa)
            ->withSuccess('Berhasil menyimpan data diagnosa');
    }
}
