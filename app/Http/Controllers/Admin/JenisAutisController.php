<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gejala;
use App\Models\Jenis_autis;
use Illuminate\Http\Request;

class JenisAutisController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Jenis_autis::orderBy('nama')->get();

        return view('admin.jenis-autis.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.jenis-autis.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:255|unique:tb_jenis_autis,nama',
            'solusi' => 'nullable',
        ]);

        Jenis_autis::create($request->all());

        return redirect()
            ->route('admin.jenis-autis.index')
            ->withSuccess('Berhasil menambah data jenis autis baru');
    }

    /**
     * Display the specified resource.
     */
    public function show(Jenis_autis $jenis_auti)
    {
        $jenis_auti->loadCount('gejala');
        
        return view('admin.jenis-autis.show', compact('jenis_auti'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Jenis_autis $jenis_auti)
    {
        return view('admin.jenis-autis.edit', compact('jenis_auti'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Jenis_autis $jenis_auti)
    {
        $request->validate([
            'nama' => 'required|string|max:255|unique:tb_jenis_autis,nama,' . $jenis_auti->id,
            'solusi' => 'nullable',
        ]);

        $jenis_auti->update($request->all());

        return redirect()
            ->route('admin.jenis-autis.show', $jenis_auti)
            ->withSuccess('Berhasil memperbarui data jenis autis');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Jenis_autis $jenis_auti)
    {
        $jenis_auti->delete();

        return redirect()
            ->route('admin.jenis-autis.index')
            ->withSuccess('Berhasil menghapus data jenis autis');
    }

    public function gejala(Jenis_autis $jenis_auti)
    {
        $gejala = Gejala::orderBy('kode')->get();
        $gejalaSaatIni = $jenis_auti->gejala()->allRelatedIds()->toArray();

        return view('admin.jenis-autis.gejala', compact('gejala', 'jenis_auti', 'gejalaSaatIni'));
    }

    public function simpanGejala(Request $request, Jenis_autis $jenis_auti)
    {
    }
}
