<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Jenis_autis;
use Illuminate\Http\Request;

class JenisAutisController extends Controller
{
    /**
     * Menambah atau memperbarui data gejala pada jenis autis
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Jenis_autis $jenis_auti
     * @return \Illuminate\Http\Response
     */
    public function gejala(Request $request, Jenis_autis $jenis_auti)
    {
        $idGejala = $request->id;

        $gejala = $jenis_auti->gejala()->allRelatedIds()->toArray();

        if (in_array($idGejala, $gejala)) {
            $jenis_auti->gejala()->detach($idGejala);
        } else {
            $jenis_auti->gejala()->attach($idGejala);
        }

        return response()
            ->json([
                'success' => true,
                'message' => 'Berhasil memperbarui data',
            ]);
    }
}
