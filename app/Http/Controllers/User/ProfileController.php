<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        return view('user.profile');
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'picture' => 'nullable|image|mimes:jpg,jpeg,png',
            'email' => 'required|email|min:12|max:255',
            'password' => 'nullable|min:8|max:255',
        ]);

        $user = auth()->user();

        if ($user->email != $request->email) {
            $user->email_verified_at = null;
            $request->user()->sendEmailVerificationNotification();
        }

        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        if ($request->hasFile('picture')) {
            $user->clearMediaCollection('profile_picture');
            $user->addMediaFromRequest('picture')
                ->toMediaCollection('profile_picture');
        }

        $user->save();

        return redirect()
            ->back()
            ->withSuccess('Berhasil memperbarui profil');
    }
}
