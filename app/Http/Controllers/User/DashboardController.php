<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Diagnosa;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $riwayat = Diagnosa::with('jenisAutis')->where('user_id', auth()->id())->latest()->take(5)->get();

        return view('user.dashboard', compact('riwayat'));
    }
}
