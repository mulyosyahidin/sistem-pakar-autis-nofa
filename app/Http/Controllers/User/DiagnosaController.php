<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Diagnosa;
use App\Models\Gejala;
use App\Models\Jenis_autis;
use App\Models\Kriteria;
use App\Models\Lokasi_pengobatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DiagnosaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Diagnosa::with('jenisAutis')->where('user_id', Auth::id())->latest()->paginate();

        return view('user.diagnosa.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $gejala = Gejala::orderByRaw("CONVERT(SUBSTRING(kode, 2), SIGNED)")->get();
        $kriteria = Kriteria::orderBy('nilai')->get();

        return view('user.diagnosa.create', compact('gejala', 'kriteria'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'nilai' => 'required|array',
        ]);


        $nilai = $request->nilai;
        session()->put('nilai_user', $nilai);

        $jenisAutis = Jenis_autis::with('gejala')->orderBy('nama')->get();
        $dataNilai = [];

        foreach ($jenisAutis as $autis) {
            $n = 1;

            foreach ($autis->gejala as $gejala) {
                $dataNilai[$autis->id][$n] = [
                    'pakar' => $gejala->bobot,
                    'user' => dataKriteria($nilai[$gejala->id])->nilai,
                    'perkalian' => $gejala->bobot * dataKriteria($nilai[$gejala->id])->nilai,
                ];

                $n++;
            }
        }

        $hasil = [];
        foreach ($jenisAutis as $autis) {
            $n = count($autis->gejala);
            $index = 0;
            $CFold = 0;

            foreach ($autis->gejala as $gejala) {
                if ($index != ($n - 1)) {
                    if ($index == 0) {
                        $CFold = $dataNilai[$autis->id][1]['perkalian'] + $dataNilai[$autis->id][2]['perkalian'] * (1 - $dataNilai[$autis->id][1]['perkalian']);
                    } else {
                        $CFold = $CFold + $dataNilai[$autis->id][$index + 2]['perkalian'] * (1 - $CFold);
                    }

                    if ($index == 0) {
                        $hasil[$autis->id] = $CFold;
                    } else {
                        if ($CFold > $hasil[$autis->id]) {
                            $hasil[$autis->id] = $CFold;
                        }
                    }
                }
                $index++;
            }
        }

        $diagnosa = Auth::user()->diagnosa()->create([
            'judul' => $request->nama,
            'id_jenis_autis' => hasilNilai($hasil)['autis']['id'],
            'nilai' => hasilNilai($hasil)['nilai'],
        ]);

        foreach ($nilai as $idGejala => $idKriteria) {
            $diagnosa->gejala()->attach($idGejala, [
                'id_kriteria' => $idKriteria,
            ]);
        }

        return redirect()
            ->route('user.diagnosa.show', $diagnosa)
            ->withSuccess('Berhasil melakukan diagnosa');
    }

    /**
     * Display the specified resource.
     */
    public function show(Diagnosa $diagnosa)
    {
        $diagnosa->load('gejala', 'jenisAutis');
        $lokasiPengobatan = Lokasi_pengobatan::all();

        return view('user.diagnosa.show', compact('diagnosa', 'lokasiPengobatan'));
    }
}
