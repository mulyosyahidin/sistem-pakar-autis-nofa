<?php

namespace Database\Seeders;

use App\Enums\UserRole;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@local.test',
            'password' => bcrypt('password'),
            'role' => UserRole::ADMIN->value,
        ]);

        \App\Models\User::factory()->create([
            'name' => 'User 1',
            'email' => 'user1@local.test',
            'password' => bcrypt('password'),
        ]);

        \App\Models\User::factory()->create([
            'name' => 'User 2',
            'email' => 'user2@local.test',
            'password' => bcrypt('password'),
        ]);
    }
}
