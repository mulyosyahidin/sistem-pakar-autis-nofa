<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tb_diagnosa', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->foreignIdFor(\App\Models\User::class)->nullable()->references('id')->on('users')->onDelete('cascade');
            $table->string('judul');
            $table->unsignedInteger('id_jenis_autis')->nullable();
            $table->double('nilai');
            $table->timestamps();

            $table->foreign('id_jenis_autis')->references('id')->on('tb_jenis_autis')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tb_diagnosa');
    }
};
