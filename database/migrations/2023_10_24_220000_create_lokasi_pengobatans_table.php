<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tb_lokasi_pengobatan', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('nama');
            $table->mediumText('alamat')->nullable();
            $table->mediumText('keterangan')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tb_lokasi_pengobatan');
    }
};
