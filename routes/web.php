<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\DiagnosaController;
use App\Http\Controllers\Admin\GejalaController;
use App\Http\Controllers\Admin\JenisAutisController;
use App\Http\Controllers\Admin\KriteriaController;
use App\Http\Controllers\Admin\LokasiPengobatanController;
use App\Http\Controllers\Admin\PerhitunganController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\User\DashboardController as UserDashboardController;
use App\Http\Controllers\User\DiagnosaController as UserDiagnosaController;
use App\Http\Controllers\User\ProfileController as UserProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth', 'role:admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/profile', [ProfileController::class, 'index'])->name('profile.index');
    Route::put('/profile', [ProfileController::class, 'update'])->name('profile.update');

    Route::resource('jenis-autis', JenisAutisController::class);
    Route::get('/jenis-autis/gejala/{jenis_auti}', [JenisAutisController::class, 'gejala'])->name('jenis-autis.gejala');
    Route::put('/jenis-autis/gejala/{jenis_auti}', [JenisAutisController::class, 'simpanGejala'])->name('jenis-autis.gejala');

    Route::resource('gejala', GejalaController::class);
    Route::resource('kriteria', KriteriaController::class)->except('show');

    Route::controller(PerhitunganController::class)->prefix('perhitungan')->as('perhitungan.')->group(function () {
        Route::get('/pilih-gejala', 'pilihGejala')->name('pilih-gejala');
        Route::post('/perhitungan', 'perhitungan')->name('perhitungan');
        Route::post('/simpan', 'simpan')->name('simpan');
    });

    Route::resource('diagnosa', DiagnosaController::class)->only('index', 'show', 'destroy');
    Route::resource('lokasi-pengobatan', LokasiPengobatanController::class);
});

Route::group(['middleware' => ['auth', 'role:user'], 'prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('/dashboard', [UserDashboardController::class, 'index'])->name('dashboard');
    Route::resource('diagnosa', UserDiagnosaController::class)->except('edit', 'update', 'destroy');

    Route::get('/profile', [UserProfileController::class, 'index'])->name('profile.index');
    Route::put('/profile', [UserProfileController::class, 'update'])->name('profile.update');
});

require __DIR__ . '/auth.php';
