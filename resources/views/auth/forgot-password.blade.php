<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Lupa Paasword?</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/vendors/feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/vendors/css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('assets/themes/skydash/images/favicon.png') }}" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                            <div class="brand-logo">
                                <img src="{{ asset('assets/themes/skydash/images/logo.svg') }}" alt="logo">
                            </div>

                            @if (session()->has('status'))
                                <div class="alert alert-info">
                                    {{ session()->get('status') }}
                                </div>
                            @endif

                            <h4>Lupa Password?</h4>
                            <h6 class="font-weight-light">Lupa password akun Anda? Masukan email akun dan kami akan
                                mengirim tautan untuk membuat password baru.</h6>
                            <form class="pt-3" method="POST" action="{{ route('password.email') }}">
                                @csrf

                                <div class="form-group">
                                    <input type="email" name="email" value="{{ old('email') }}"
                                        class="form-control form-control-lg @error('email') is-invalid @enderror"
                                        id="exampleInputEmail1" placeholder="Email" required>

                                    @error('email')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                                <div class="mt-3">
                                    <input class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                                        type="submit" value="Reset Password" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset('assets/themes/skydash/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('assets/themes/skydash/js/off-canvas.js') }}"></script>
    <script src="{{ asset('assets/themes/skydash/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('assets/themes/skydash/js/template.js') }}"></script>
    <script src="{{ asset('assets/themes/skydash/js/settings.js') }}"></script>
    <script src="{{ asset('assets/themes/skydash/js/todolist.js') }}"></script>
    <!-- endinject -->
</body>

</html>
