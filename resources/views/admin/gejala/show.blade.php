@extends('layouts.admin')
@section('title', 'Data Gejala')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Data Gejala</h3>

                    <a href="{{ route('admin.gejala.index') }}" class="btn btn-primary btn-sm">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Gejala</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>ID</td>
                                        <td><strong>{{ $gejala->id }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Kode</td>
                                        <td><strong>{{ $gejala->kode }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Bobot</td>
                                        <td><strong>{{ $gejala->bobot }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Gejala</td>
                                        <td><strong>{{ $gejala->gejala }}</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="{{ route('admin.gejala.edit', $gejala) }}" class="btn btn-warning mr-1 btn-sm">Edit</a>
                        <a href="#" class="btn btn-danger btn-delete btn-sm">Hapus</a>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-body">
                        <h4 class="card-title">Jenis Autis</h4>
                        <p>Jenis autis yang memiliki gejala ini</p>
                        <div class="table-responsive">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($gejala->jenisAutis as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->nama }}</td>
                                            <td class="d-flex justify-content-end">
                                                <a href="{{ route('admin.jenis-autis.show', $item) }}" class="btn btn-sm btn-success">Lihat</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_html')
    <form action="{{ route('admin.gejala.destroy', $gejala) }}" method="post" id="delete-form">
        @csrf
        @method('DELETE')
    </form>
@endsection

@push('custom_js')
    <script>
        let deleteBtn = document.querySelector('.btn-delete');
        deleteBtn.addEventListener('click', () => {

            Swal.fire({
                title: 'Yakin Ingin Menghapus?',
                text: "Yakin ingin menghapus data? Data yang dihapus tidak bisa dikembalikan lagi.",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#3085d6',
                confirmButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    let form = document.querySelector('#delete-form');
                    form.submit();
                }
            })
        });
    </script>
@endpush
