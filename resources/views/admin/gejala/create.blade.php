@extends('layouts.admin')
@section('title', 'Tambah Data Gejala')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Tambah Data Gejala</h3>

                    <a href="{{ route('admin.gejala.index') }}" class="btn btn-primary btn-sm">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="{{ route('admin.gejala.store') }}" method="post">
                        <div class="card-body">
                            <h4 class="card-title">Data Gejala</h4>

                            @csrf

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="kode">Kode</label>
                                        <input type="text" name="kode"
                                            class="form-control @error('kode') is-invalid @enderror" id="kode"
                                            value="{{ old('kode') }}" placeholder="Kode" maxlength="4" required>

                                        @error('kode')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="bobot">Bobot</label>
                                        <input type="text" name="bobot"
                                            class="form-control @error('bobot') is-invalid @enderror" id="bobot"
                                            value="{{ old('bobot') }}" placeholder="Bobot" maxlength="255" required>

                                        @error('bobot')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="gejala">Gejala</label>
                                <textarea name="gejala" class="form-control @error('gejala') is-invalid @enderror" id="gejala" placeholder="Gejala" rows="5"
                                    required>{{ old('gejala') }}</textarea>

                                @error('gejala')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <input type="submit" value="Simpan" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
