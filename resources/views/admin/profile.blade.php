@extends('layouts.admin')
@section('title', 'Profil Saya')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="d-flex justify-content-center h-100">
                                    <div class="align-self-center text-center">
                                        <img src="{{ auth()->user()->getProfilePicture() }}" alt="Foto Profil"
                                            class="img-lg rounded-circle mb-3" />
                                        <div class="mb-3">
                                            <h3>{{ auth()->user()->name }}</h3>
                                        </div>
                                        <p>{{ auth()->user()->email }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="card">
                                    <form action="{{ route('admin.profile.update') }}" method="post"
                                        enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')

                                        <div class="card-body">
                                            <h4 class="card-title">Edit Profil</h4>

                                            @if (auth()->user()->email_verified_at == null)
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    Email belum terverifikasi. Kami telah mengirim tautan konfirmasi ke
                                                    email tersebut.

                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif

                                            <div class="form-group">
                                                <label for="nama">Nama</label>
                                                <input type="text" name="name"
                                                    class="form-control @error('name') is-invalid @enderror" id="nama"
                                                    value="{{ old('name', auth()->user()->name) }}" placeholder="Nama"
                                                    minlength="4" maxlength="255" required>

                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="file">Foto Profil</label>
                                                <input type="file" name="picture"
                                                    class="form-control @error('picture') is-invalid @enderror"
                                                    id="file">

                                                @error('picture')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="row">
                                                <div class="col-12 col-md-6">
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" name="email"
                                                            class="form-control @error('email') is-invalid @enderror"
                                                            id="nama"
                                                            value="{{ old('email', auth()->user()->email) }}"
                                                            placeholder="Email" minlength="12" maxlength="255" required>

                                                        @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    <div class="form-group">
                                                        <label for="nama">Password</label>
                                                        <input type="password" name="password"
                                                            class="form-control @error('password') is-invalid @enderror"
                                                            id="password" value="{{ old('password') }}"
                                                            placeholder="Password" minlength="8" maxlength="255">

                                                        @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="d-flex justify-content-end">
                                                <input type="submit" class="btn btn-primary" value="Simpan">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
