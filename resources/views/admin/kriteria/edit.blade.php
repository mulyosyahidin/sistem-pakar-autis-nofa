@extends('layouts.admin')
@section('title', 'Edit Data Kriteria')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Edit Data Kriteria</h3>

                    <a href="{{ route('admin.kriteria.index') }}" class="btn btn-primary btn-sm">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="{{ route('admin.kriteria.update', $kriterium) }}" method="post">
                        <div class="card-body">
                            <h4 class="card-title">Data Kriteria</h4>

                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="keterangan">Keterangan</label>
                                        <input type="text" name="keterangan"
                                            class="form-control @error('keterangan') is-invalid @enderror" id="keterangan"
                                            value="{{ old('keterangan', $kriterium->keterangan) }}" placeholder="Keterangan" required>

                                        @error('keterangan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="nilai">Nilai User</label>
                                        <input type="text" name="nilai"
                                            class="form-control @error('nilai') is-invalid @enderror" id="nilai"
                                            value="{{ old('nilai', $kriterium->nilai) }}" placeholder="Nilai User" required>

                                        @error('nilai')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <input type="submit" value="Simpan" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
