@extends('layouts.admin')
@section('title', 'Kelola Data Kriteria')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Kelola Data Kriteria</h3>

                    <a href="{{ route('admin.kriteria.create') }}" class="btn btn-primary btn-sm">
                        Tambah
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Daftar Kriteria</h4>
                        <div class="table-responsive">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Keterangan</th>
                                        <th>Nilai User</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->keterangan }}</td>
                                            <td>{{ $item->nilai }}</td>
                                            <td class="d-flex justify-content-end">
                                                <a href="{{ route('admin.kriteria.edit', $item) }}"
                                                    class="btn btn-sm btn-warning mx-1">Edit</a>
                                                <a href="#" class="btn btn-sm btn-danger btn-delete"
                                                    data-id="{{ $item->id }}">Hapus</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_html')
    <form action="#" method="post" id="delete-form">
        @csrf
        @method('DELETE')
    </form>
@endsection

@push('custom_js')
    <script>
        let deleteBtns = document.querySelectorAll('.btn-delete');
        deleteBtns.forEach((btn) => {
            btn.addEventListener('click', () => {
                let id = btn.getAttribute('data-id');

                Swal.fire({
                    title: 'Yakin Ingin Menghapus?',
                    text: "Yakin ingin menghapus data? Data yang dihapus tidak bisa dikembalikan lagi.",
                    icon: 'warning',
                    showCancelButton: true,
                    cancelButtonColor: '#3085d6',
                    confirmButtonColor: '#d33',
                    confirmButtonText: 'Hapus',
                    cancelButtonText: 'Batal',
                }).then((result) => {
                    if (result.isConfirmed) {
                        let form = document.querySelector('#delete-form');
                        form.setAttribute('action',
                            `{{ route('admin.kriteria.destroy', false) }}/${id}`);

                        form.submit();
                    }
                })
            });
        });
    </script>
@endpush
