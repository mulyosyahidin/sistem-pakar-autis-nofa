@extends('layouts.admin')
@section('title', 'Pilih Gejala')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Pilih Gejala</h3>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                {{ $errors->first() }}
            </div>
        @endif

        <div class="row">
            <div class="col-12">
                <form action="{{ route('admin.perhitungan.perhitungan') }}" method="post">
                    @csrf

                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Daftar Gejala</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode</th>
                                            <th>Gejala</th>
                                            <th>Nilai CF User</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($gejala as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->kode }}</td>
                                                <td>{{ \Str::limit($item->gejala, 120) }}</td>
                                                <td class="d-flex justify-content-end">
                                                    <select name="nilai[{{ $item->id }}]" class="form-control" required>
                                                        <option selected disabled>Pilih Kriteria</option>
                                                        @foreach ($kriteria as $item)
                                                            <option value="{{ $item->id }}">{{ $item->keterangan }}
                                                                ({{ $item->nilai }})
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <input type="submit" value="Selanjutnya" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
