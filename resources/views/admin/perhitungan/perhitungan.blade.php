@extends('layouts.admin')
@section('title', 'Perhitungan')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Perhitungan</h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                @php($hasilNilai = [])
                @foreach ($jenisAutis as $autis)
                    <div class="card mb-3">
                        <div class="card-body">
                            <h4 class="card-title">Kemungkinan: {{ $autis->nama }}</h4>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode Gejala</th>
                                            <th>CF Pakar (H)</th>
                                            <th>CF User (E)</th>
                                            <th>CF (H&times;E)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($autis->gejala as $gejala)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $gejala->kode }}</td>
                                                <td>{{ $gejala->bobot }}</td>
                                                <td>{{ dataKriteria($nilai[$gejala->id])->nilai }}</td>
                                                <td>{{ dataKriteria($nilai[$gejala->id])->nilai * $gejala->bobot }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive">
                                <table class="table">
                                    @foreach ($autis->gejala as $gejala)
                                        @if (!$loop->last)
                                            <tr>
                                                <td class="text-right">
                                                    @if ($loop->index == 0)
                                                        CF<sub>combine</sub> CF [H, E]<sub>1, 2</sub>
                                                    @else
                                                        CF<sub>combine</sub> CF [H, E]<sub>old {{ $loop->index }},
                                                            {{ $loop->index + 2 }}</sub>
                                                    @endif
                                                </td>
                                                <td>=</td>
                                                <td>
                                                    @if ($loop->index == 0)
                                                        CF [H, E]<sub>1</sub> + CF [H, E]<sub>2</sub> &times; (1 - CF [H,
                                                        E]<sub>1</sub>)
                                                    @else
                                                        CF [H, E]<sub>old {{ $loop->index }}</sub> + CF [H,
                                                        E]<sub>{{ $loop->index + 2 }}</sub> &times; (1 - CF [H, E]<sub>old
                                                            {{ $loop->index }}</sub>)
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>=</td>
                                                <td>
                                                    @if ($loop->index == 0)
                                                        {{ $dataNilai[$autis->id][1]['perkalian'] }} +
                                                        {{ $dataNilai[$autis->id][2]['perkalian'] }} &times;
                                                        (1 - {{ $dataNilai[$autis->id][1]['perkalian'] }})
                                                    @else
                                                        {{ $CFold }} +
                                                        {{ $dataNilai[$autis->id][$loop->index + 2]['perkalian'] }}
                                                        &times;
                                                        (1 - {{ $CFold }})
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>=</td>
                                                <td>
                                                    @if ($loop->index == 0)
                                                        {{ $dataNilai[$autis->id][1]['perkalian'] }} +
                                                        {{ $dataNilai[$autis->id][2]['perkalian'] }} &times;
                                                        ({{ 1 - $dataNilai[$autis->id][1]['perkalian'] }})
                                                    @else
                                                        {{ $CFold }} +
                                                        {{ $dataNilai[$autis->id][$loop->index + 2]['perkalian'] }}
                                                        &times;
                                                        ({{ 1 - $CFold }})
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>=</td>
                                                <td>
                                                    @if ($loop->index == 0)
                                                        {{ $dataNilai[$autis->id][1]['perkalian'] }} +
                                                        {{ $dataNilai[$autis->id][2]['perkalian'] * (1 - $dataNilai[$autis->id][1]['perkalian']) }}
                                                    @else
                                                        {{ $CFold }} +
                                                        {{ $dataNilai[$autis->id][$loop->index + 2]['perkalian'] * (1 - $CFold) }}
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">CF old<sub>1</sub></td>
                                                <td>=</td>
                                                <td>
                                                    @if ($loop->index == 0)
                                                        {{ $CFold = $dataNilai[$autis->id][1]['perkalian'] + $dataNilai[$autis->id][2]['perkalian'] * (1 - $dataNilai[$autis->id][1]['perkalian']) }}
                                                    @else
                                                        {{ $CFold = $CFold + $dataNilai[$autis->id][$loop->index + 2]['perkalian'] * (1 - $CFold) }}
                                                    @endif
                                                </td>
                                            </tr>

                                            @if ($loop->index == 0)
                                                @php($hasilNilai[$autis->id] = $CFold)
                                            @else
                                                @if ($CFold > $hasilNilai[$autis->id])
                                                    @php($hasilNilai[$autis->id] = $CFold)
                                                @endif
                                            @endif
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Hasil Diagnosa</h4>

                        Hasil perhitungan diagnosa pasien terhadap {{ count($jenisAutis) }} kemungkinan autis menunjukkan
                        bahwa pasien terindikasi autis <b>{{ hasilNilai($hasilNilai)['autis']['nama'] }}</b> dengan nilai
                        <b>{{ hasilNilai($hasilNilai)['nilai'] }}</b>.
                    </div>
                    <form action="{{ route('admin.perhitungan.simpan') }}" method="post">
                        @csrf

                        <input type="hidden" name="nilai" value="{{ hasilNilai($hasilNilai)['nilai'] }}">
                        <input type="hidden" name="id_jenis_autis" value="{{ hasilNilai($hasilNilai)['autis']['id'] }}">

                        <div class="card-footer d-flex justify-content-end">
                            <input type="submit" value="Simpan" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
