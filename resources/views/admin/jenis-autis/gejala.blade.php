@extends('layouts.admin')
@section('title', 'Gejala Jenis Autis - ' . $jenis_auti->nama)

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Gejala <i>{{ $jenis_auti->nama }}</i></h3>

                    <a href="{{ route('admin.jenis-autis.show', $jenis_auti) }}" class="btn btn-primary btn-sm">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Daftar Gejala</h4>
                        <div class="table-responsive">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kode</th>
                                        <th>Gejala</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($gejala as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->kode }}</td>
                                            <td>{{ \Str::limit($item->gejala, 120) }}</td>
                                            <td class="d-flex justify-content-end">
                                                <div class="form-check form-check-primary">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="form-check-input"
                                                            data-id="{{ $item->id }}"
                                                            {{ in_array($item->id, $gejalaSaatIni) ? 'checked' : '' }}>
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('custom_js')
    <script>
        let checkbox = document.querySelectorAll('.form-check-input');

        checkbox.forEach((box) => {
            box.addEventListener('click', () => {
                let id = box.getAttribute('data-id');

                let formBody = new FormData();
                formBody.append('id', id);

                fetch(`{{ route('api.jenis-autis.gejala', $jenis_auti) }}`, {
                        'method': 'POST',
                        'body': formBody,
                    })
                    .then(res => res.json())
                    .then(res => {
                        if (res.success) {
                            Toastify({
                                text: res.message,
                                duration: 3000,
                                gravity: "top", // `top` or `bottom`
                                position: "right", // `left`, `center` or `right`
                                stopOnFocus: true, // Prevents dismissing of toast on hover
                                style: {
                                    background: "linear-gradient(to right, #00b09b, #96c93d)",
                                },
                            }).showToast();
                        }
                    })
                    .catch(error => {
                        console.log(error);

                        Swal.fire({
                            icon: 'error',
                            title: 'Terjadi Kesalahan',
                            text: 'Terjadi kesalahan tidak terduga. Silahkan periksa developer console.',
                        });
                    });
            });
        });
    </script>
@endpush
