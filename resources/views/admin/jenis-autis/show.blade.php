@extends('layouts.admin')
@section('title', 'Data Jenis Autis')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Data Jenis Autis</h3>

                    <a href="{{ route('admin.jenis-autis.index') }}" class="btn btn-primary btn-sm">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Jenis Autis</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>ID</td>
                                        <td><strong>{{ $jenis_auti->id }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td><strong>{{ $jenis_auti->nama }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Solusi</td>
                                        <td><strong>{{ $jenis_auti->solusi }}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Gejala</td>
                                        <td><strong>{{ $jenis_auti->gejala_count }}</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="{{ route('admin.jenis-autis.edit', $jenis_auti) }}"
                            class="btn btn-warning mr-1 btn-sm">Edit</a>
                        <a href="#" class="btn btn-danger btn-delete btn-sm">Hapus</a>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-header d-flex justify-content-between">
                        <h4 class="card-title">Gejala</h4>

                        <a href="{{ route('admin.jenis-autis.gejala', $jenis_auti) }}"
                            class="btn btn-primary btn-sm btn-30">
                            Edit
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kode</th>
                                        <th>Gejala</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($jenis_auti->gejala as $gejala)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $gejala->kode }}</td>
                                            <td>{{ \Str::limit($gejala->gejala, 120) }}</td>
                                            <td>
                                                <a href="{{ route('admin.gejala.show', $gejala) }}" class="btn btn-sm btn-success">Lihat</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_html')
    <form action="{{ route('admin.jenis-autis.destroy', $jenis_auti) }}" method="post" id="delete-form">
        @csrf
        @method('DELETE')
    </form>
@endsection

@push('custom_js')
    <script>
        let deleteBtn = document.querySelector('.btn-delete');
        deleteBtn.addEventListener('click', () => {

            Swal.fire({
                title: 'Yakin Ingin Menghapus?',
                text: "Yakin ingin menghapus data? Data yang dihapus tidak bisa dikembalikan lagi.",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#3085d6',
                confirmButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    let form = document.querySelector('#delete-form');
                    form.submit();
                }
            })
        });
    </script>
@endpush
