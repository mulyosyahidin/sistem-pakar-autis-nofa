@extends('layouts.admin')
@section('title', 'Tambah Data Jenis Autis')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Tambah Data Jenis Autis</h3>

                    <a href="{{ route('admin.jenis-autis.index') }}" class="btn btn-primary btn-sm">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="{{ route('admin.jenis-autis.store') }}" method="post">
                        <div class="card-body">
                            <h4 class="card-title">Data Jenis Autis</h4>

                            @csrf

                            <div class="form-group">
                                <label for="nama">Nama</label>
                                <input type="text" name="nama"
                                    class="form-control @error('nama') is-invalid @enderror" id="nama"
                                    value="{{ old('nama') }}" placeholder="Nama" minlength="4" maxlength="255" required>

                                @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="solusi">Solusi</label>
                                <textarea name="solusi" class="form-control @error('solusi') is-invalid @enderror" id="solusi" placeholder="Solusi" rows="5"
                                    required>{{ old('solusi') }}</textarea>

                                @error('solusi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <input type="submit" value="Simpan" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
