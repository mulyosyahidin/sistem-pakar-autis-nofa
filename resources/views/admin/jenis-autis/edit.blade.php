@extends('layouts.admin')
@section('title', 'Edit Data Jenis Autis')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Edit Data Jenis Autis</h3>

                    <a href="{{ route('admin.jenis-autis.index') }}" class="btn btn-primary btn-sm">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="{{ route('admin.jenis-autis.update', $jenis_auti) }}" method="post">
                        <div class="card-body">
                            <h4 class="card-title">Data Jenis Autis</h4>

                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="nama">Nama</label>
                                <input type="text" name="nama"
                                    class="form-control @error('nama') is-invalid @enderror" id="nama"
                                    value="{{ old('nama', $jenis_auti->nama) }}" placeholder="Nama" minlength="4" maxlength="255" required>

                                @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="solusi">Solusi</label>
                                <textarea name="solusi" class="form-control @error('solusi') is-invalid @enderror" id="solusi" placeholder="Solusi" rows="5"
                                    required>{{ old('solusi', $jenis_auti->solusi) }}</textarea>

                                @error('solusi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <input type="submit" value="Simpan" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
