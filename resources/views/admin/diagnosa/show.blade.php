@extends('layouts.admin')
@section('title', 'Data Hasil Diagnosa')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Data Hasil Diagnosa</h3>

                    <a href="{{ route('admin.diagnosa.index') }}" class="btn btn-primary btn-sm">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table no-border-top">
                            <tbody>
                                <tr>
                                    <td>ID</td>
                                    <td><strong>{{ $diagnosa->id }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Judul</td>
                                    <td><strong>{{ $diagnosa->judul }}</strong></td>
                                </tr>
                                <tr>
                                    <td>User</td>
                                    <td><strong>{{ $diagnosa->user->name }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Hasil</td>
                                    <td><strong>{{ $diagnosa->jenisAutis->nama }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Nilai</td>
                                    <td><strong>{{ $diagnosa->nilai }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Tanggal</td>
                                    <td><strong>{{ $diagnosa->created_at->translatedFormat('l, d F Y H:i') }}</strong>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="#" class="btn btn-danger btn-delete btn-sm">Hapus</a>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-body">
                        <h4 class="card-title">Gejala</h4>
                        <p>Gejala yang dipilih saat melakukan diagnosa</p>
                        <div class="table-responsive">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Gejala</th>
                                        <th>Kode</th>
                                        <th>Kriteria</th>
                                        <th>Nilai CF</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($diagnosa->gejala as $gejala)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ \Str::limit($gejala->gejala, 120) }}</td>
                                            <td>{{ $gejala->kode }}</td>
                                            <td>{{ $gejala->pivot->kriteria->keterangan }}</td>
                                            <td>{{ $gejala->pivot->kriteria->nilai }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_html')
    <form action="{{ route('admin.diagnosa.destroy', $diagnosa) }}" method="post" id="delete-form">
        @csrf
        @method('DELETE')
    </form>
@endsection

@push('custom_js')
    <script>
        let deleteBtn = document.querySelector('.btn-delete');
        deleteBtn.addEventListener('click', () => {

            Swal.fire({
                title: 'Yakin Ingin Menghapus?',
                text: "Yakin ingin menghapus data? Data yang dihapus tidak bisa dikembalikan lagi.",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#3085d6',
                confirmButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    let form = document.querySelector('#delete-form');
                    form.submit();
                }
            })
        });
    </script>
@endpush
