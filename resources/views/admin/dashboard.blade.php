@extends('layouts.admin')
@section('title', 'Admin Dashboard')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin">
                <h3 class="font-weight-bold">Selamat Datang {{ auth()->user()->name }}</h3>
                <h6 class="font-weight-normal mb-0">
                    Sistem Pakar Diagnosa Autisme
                </h6>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-3 mb-4 stretch-card transparent">
                <div class="card card-tale">
                    <div class="card-body">
                        <p class="mb-4">Jenis Autis</p>
                        <p class="fs-30 mb-2">{{ $count['jenis_autis'] }}</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 mb-4 stretch-card transparent">
                <div class="card card-dark-blue">
                    <div class="card-body">
                        <p class="mb-4">Gejala</p>
                        <p class="fs-30 mb-2">{{ $count['gejala'] }}</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 mb-4 stretch-card transparent">
                <div class="card card-light-blue">
                    <div class="card-body">
                        <p class="mb-4">User</p>
                        <p class="fs-30 mb-2">{{ $count['user'] }}</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 mb-4 stretch-card transparent">
                <div class="card card-dark-blue">
                    <div class="card-body">
                        <p class="mb-4">Diagnosa</p>
                        <p class="fs-30 mb-2">{{ $count['diagnosa'] }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <p class="card-title">Perhitungan Terbaru</p>

                            @if (count($diagnosa) > 5)
                                <a href="{{ route('admin.diagnosa.index') }}" class="btn btn-sm btn-primary btn-30">Lihat
                                    Semua
                                    &rarr;</a>
                            @endif
                        </div>

                        @if (count($diagnosa) > 0)
                            <div class="table-responsive">
                                <table class="table table-striped table-borderless">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>User</th>
                                            <th>Tanggal</th>
                                            <th>Hasil</th>
                                            <th>Nilai</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($diagnosa as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->user->name }}</td>
                                                <td>{{ $item->created_at->translatedFormat('l, d F Y H:i') }}</td>
                                                <td>{{ $item->jenisAutis->nama }}</td>
                                                <td>{{ $item->nilai }}</td>
                                                <td class="text-right">
                                                    <a href="{{ route('admin.diagnosa.show', $item) }}"
                                                        class="btn btn-primary btn-sm">Lihat</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="alert alert-info">
                                Tidak ada data untuk ditampilkan.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('custom_js')
    <script src="{{ asset('assets/themes/skydash/js/dashboard.js') }}"></script>
@endpush
