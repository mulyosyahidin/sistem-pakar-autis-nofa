@extends('layouts.admin')
@section('title', 'Data Lokasi Pengobatan')

@section('custom_head')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
        integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin="" />

    <style>
        #map {
            height: 600px;
            border-radius: 8px
        }
    </style>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="d-flex justify-content-between">
                    <h3 class="font-weight-bold">Data Lokasi Pengobatan</h3>

                    <a href="{{ route('admin.lokasi-pengobatan.index') }}" class="btn btn-primary btn-sm">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table no-border-top">
                            <tbody>
                                <tr>
                                    <td>ID</td>
                                    <td><strong>{{ $lokasi_pengobatan->id }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td><strong>{{ $lokasi_pengobatan->nama }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td><strong>{{ $lokasi_pengobatan->alamat }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Keterangan</td>
                                    <td><strong>{{ $lokasi_pengobatan->keterangan }}</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="{{ route('admin.lokasi-pengobatan.edit', $lokasi_pengobatan) }}"
                            class="btn btn-warning mr-1 btn-sm">Edit</a>
                        <a href="#" class="btn btn-danger btn-delete btn-sm">Hapus</a>
                    </div>
                </div>

                <div class="card mt-3">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_html')
    <form action="{{ route('admin.lokasi-pengobatan.destroy', $lokasi_pengobatan) }}" method="post" id="delete-form">
        @csrf
        @method('DELETE')
    </form>
@endsection

@push('custom_js')
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
        integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>

    <script>
        let deleteBtn = document.querySelector('.btn-delete');
        deleteBtn.addEventListener('click', () => {

            Swal.fire({
                title: 'Yakin Ingin Menghapus?',
                text: "Yakin ingin menghapus data? Data yang dihapus tidak bisa dikembalikan lagi.",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#3085d6',
                confirmButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    let form = document.querySelector('#delete-form');
                    form.submit();
                }
            })
        });
    </script>

    <script>
        var map = L.map('map').setView([{{ $lokasi_pengobatan->latitude }}, {{ $lokasi_pengobatan->longitude }}], 14);
        var marker = L.marker([{{ $lokasi_pengobatan->latitude }}, {{ $lokasi_pengobatan->longitude }}]).addTo(map);

        map.attributionControl.setPrefix('')
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);
    </script>
@endpush
