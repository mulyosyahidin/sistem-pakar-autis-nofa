<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Sistem Pakar Diagnosa Autisme</title>

    <!-- Theme favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/icon.png') }}">

    <!--Swiper slider css-->
    <link href="{{ asset('assets/themes/prompt/libs/swiper/swiper-bundle.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Animation on Scroll css -->
    <link href="{{ asset('assets/themes/prompt/libs/aos/aos.css') }}" rel="stylesheet" type="text/css">

    <!-- Style css -->
    <link href="{{ asset('assets/themes/prompt/css/style.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Icons css -->
    <link href="{{ asset('assets/themes/prompt/css/icons.min.css') }}" rel="stylesheet" type="text/css">
</head>

<body class="text-gray-800" style="background: white;">
    <!-- =========== Navbar Start =========== -->
    <header id="navbar"
        class="light fixed top-0 inset-x-0 flex items-center z-40 w-full lg:bg-transparent bg-white transition-all py-5">
        <div class="container">
            <nav class="flex items-center">
                <!-- Navbar Brand Logo -->
                <a href="{{ route('home') }}">
                    <img src="{{ asset('assets/images/logo-dark.png') }}" class="h-8 logo-dark"
                        alt="Logo Dark">
                    <img src="{{ asset('assets/images/logo.png') }}" class="h-8 logo-light"
                        alt="Logo Light">
                </a>

                <!-- Nevigation Menu -->
                <div class="hidden lg:block ms-auto"></div>

                <!-- Download Button -->
                <div class="hidden lg:flex items-center ms-3 gap-1">
                    @if (auth()->check())
                        @if (auth()->user()->role == \App\Enums\UserRole::ADMIN->value)
                            <a href="{{ route('admin.dashboard') }}"
                                class="bg-primary text-white px-4 py-2 rounded inline-flex items-center text-sm">Dashboard</a>
                        @else
                            <a href="{{ route('user.dashboard') }}"
                                class="bg-primary text-white px-4 py-2 rounded inline-flex items-center text-sm">Dashboard</a>
                        @endif
                    @else
                        <a href="{{ route('register') }}"
                            class="bg-black/70 text-white rounded-md text-sm flex-none focus:outline focus:outline-black/50 px-4 py-2 transition duration-300 ease-in-out hover:bg-black hover:text-white">
                            Daftar
                        </a>
                        <a href="{{ route('login') }}"
                            class="bg-primary text-white px-4 py-2 rounded inline-flex items-center text-sm transition duration-300 ease-in-out hover:bg-primary-dark hover:text-white">
                            Login
                        </a>
                    @endif
                </div>

                <!-- Moblie Menu Toggle Button (Offcanvas Button) -->
                <div class="lg:hidden flex items-center ms-auto px-2.5">
                    <button type="button" data-fc-target="mobileMenu" data-fc-type="offcanvas">
                        <i class="fa-solid fa-bars text-2xl text-gray-500"></i>
                    </button>
                </div>
            </nav>
        </div>
    </header>
    <!-- =========== Navbar End =========== -->

    <!-- =========== Mobile Menu Start (Offcanvas) =========== -->
    <div id="mobileMenu"
        class="fc-offcanvas-open:translate-x-0 translate-x-full fixed top-0 end-0 transition-all duration-200 transform h-full w-full max-w-md z-50 bg-white border-s hidden">
        <div class="flex flex-col h-full divide-y-2 divide-gray-200">
            <!-- Mobile Menu Topbar Logo (Header) -->
            <div class="p-6 flex items-center justify-between">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('assets/themes/prompt/images/logo-dark.png') }}" class="h-8" alt="Logo">
                </a>

                <button data-fc-dismiss class="flex items-center px-2">
                    <i class="fa-solid fa-xmark text-xl"></i>
                </button>
            </div>

            <!-- Mobile Menu Download Button (Footer) -->
            <div class="p-6 flex items-center justify-center">
                @if (auth()->check())
                    @if (auth()->user()->role == \App\Enums\UserRole::ADMIN->value)
                        <a href="{{ route('admin.dashboard') }}"
                            class="bg-primary w-full text-white p-3 rounded flex items-center justify-center text-sm">Dashboard</a>
                    @else
                        <a href="{{ route('user.dashboard') }}"
                            class="bg-primary w-full text-white p-3 rounded flex items-center justify-center text-sm">Dashboard</a>
                    @endif
                @else
                    <a href="{{ route('login') }}"
                        class="bg-primary w-full text-white p-3 rounded flex items-center justify-center text-sm">Login</a>
                @endif
            </div>
        </div>
    </div>
    <!-- =========== Mobile Menu End =========== -->

    <!-- =========== Hero Section Start =========== -->
    <section class="bg-gradient-to-t from-gray-200/70 relative">

        <section id="" class="relative py-16 sm:py-20 md:py-40">
            <div class="container">
                <div class="grid lg:grid-cols-2 grid-cols-1 gap-10 items-center">

                    <div class="order-2 lg:order-1">
                        <div class="text-center lg:text-start">
                            <h1 class="text-3xl/tight sm:text-4xl/tight lg:text-5xl/tight font-semibold mb-7">
                                Sistem Pakar Diagnosa
                                <span
                                    class="relative z-0 after:bg-yellow-100 after:-z-10 after:absolute after:h-6 after:w-full after:bottom-0 after:end-0">Autisme</span>.
                            </h1>
                            <p class="text-gray-500 leading-relaxed lg:w-3/4">
                                Sistem pakar diagnosa autisme adalah sistem yang dapat membantu mendiagnosa autisme
                                pada anak-anak. Sistem ini dibuat dengan menggunakan metode Certainty Factor (CF).
                            </p>

                            @if (auth()->check())
                                <div class="mt-10 flex items-center justify-center lg:justify-start">
                                    <a href="{{ auth()->user()->role == \App\Enums\UserRole::ADMIN->value ? route('admin.dashboard') : route('user.dashboard') }}"
                                        class="bg-black/70 text-white rounded-md text-sm font-semibold flex-none shadow shadow-black hover:shadow-lg hover:shadow-black/30 focus:shadow-none focus:outline focus:outline-black/50 px-8 py-3">Buka
                                        Dashboard <i class="fa-solid fa-arrow-right ms-2"></i></a>
                                </div>
                            @else
                                <div class="mt-10 flex items-center justify-center lg:justify-start">
                                    <a href="{{ route('register') }}"
                                        class="bg-black/70 text-white rounded-md text-sm font-semibold flex-none shadow shadow-black hover:shadow-lg hover:shadow-black/30 focus:shadow-none focus:outline focus:outline-black/50 px-8 py-3">Daftar
                                        <i class="fa-solid fa-arrow-right ms-2"></i></a>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="order-1 lg:order-2" data-aos="zoom-in">
                        <div class="grid grid-cols-2 items-center gap-6">

                            <div>
                                <img src="{{ asset('assets/images/1.jpeg') }}" alt="photo-12"
                                    class="border-[6px] border-white bg-white shadow-md">
                            </div>

                            <div>
                                <div class="grid grid-rows-2 items-center gap-6">
                                    <div>
                                        <img src="{{ asset('assets/images/3.jpg') }}" alt="photo-14"
                                            class="border-[6px] border-white bg-white shadow-md">
                                    </div>

                                    <div>
                                        <img src="{{ asset('assets/images/2.jpeg') }}" alt="photo-15"
                                            class="border-[6px] border-white bg-white shadow-md">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- bottom rounded img -->
        <div class="absolute -bottom-1 w-full">
            <svg class="w-full h-full" viewBox="0 0 1440 40" version="1.1" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="shape-b" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="curve" fill="#fff">
                        <path
                            d="M0,30.013 C239.659,10.004 479.143,0 718.453,0 C957.763,0 1198.28,10.004 1440,30.013 L1440,40 L0,40 L0,30.013 Z"
                            id="Path"></path>
                    </g>
                </g>
            </svg>
        </div>

    </section>
    <!-- =========== Hero Section End =========== -->

    <!-- Frost Plugin Js -->
    <script src="{{ asset('assets/themes/prompt/libs/@frostui/tailwindcss/frostui.js') }}"></script>

    <!-- Swiper Plugin Js -->
    <script src="{{ asset('assets/themes/prompt/libs/swiper/swiper-bundle.min.js') }}"></script>

    <!-- Animation on Scroll Plugin Js -->
    <script src="{{ asset('assets/themes/prompt/libs/aos/aos.js') }}"></script>

    <!-- Theme Js -->
    <script src="{{ asset('assets/themes/prompt/js/theme.min.js') }}"></script>

</body>

</html>
