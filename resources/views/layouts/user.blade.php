<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('title')</title>

    <!-- Theme favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/themes/prompt/images/favicon.ico') }}">

    <!--Swiper slider css-->
    <link href="{{ asset('assets/themes/prompt/libs/swiper/swiper-bundle.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Animation on Scroll css -->
    <link href="{{ asset('assets/themes/prompt/libs/aos/aos.css') }}" rel="stylesheet" type="text/css">

    <!-- Style css -->
    <link href="{{ asset('assets/themes/prompt/css/style.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Icons css -->
    <link href="{{ asset('assets/themes/prompt/css/icons.min.css') }}" rel="stylesheet" type="text/css">

    @yield('custom_head')

    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    @include('layouts.includes.user.header')

    @yield('content')

    <form action="{{ route('logout') }}" method="post" id="logout-form">
        @csrf
    </form>

    <a href="{{ route('user.diagnosa.create') }}" title="Diagnosa Baru"
        class="fixed text-sm rounded-full z-10 bottom-8 end-5 h-10 w-10 text-center bg-primary/20 text-primary flex justify-center items-center">
        <i class="fa-solid fa-plus text-base"></i>
    </a>

    @yield('custom_html')

    <!-- Frost Plugin Js -->
    <script src="{{ asset('assets/themes/prompt/libs/@frostui/tailwindcss/frostui.js') }}"></script>

    <!-- Theme Js -->
    <script src="{{ asset('assets/themes/prompt/js/theme.min.js') }}"></script>

    <script src="{{ asset('assets/themes/skydash/vendors/sweetalert/sweetalert.min.js') }}"></script>

    @if (session()->has('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Berhasil!',
                text: `{{ session()->get('success') }}`,
            })
        </script>
    @endif

    @if (session()->has('error'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Terjadi kesalahan!',
                text: `{{ session()->get('error') }}`,
            })
        </script>
    @endif

    @stack('custom_js')

    <script>
        let logoutLink = document.querySelector('.logout-link');
        let logoutForm = document.querySelector('#logout-form');

        logoutLink.addEventListener('click', function(e) {
            e.preventDefault();
            logoutForm.submit();
        });
    </script>
</body>

</html>
