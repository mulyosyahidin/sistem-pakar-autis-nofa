<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item {{ activeClass('admin.dashboard') }}">
            <a class="nav-link" href="{{ route('admin.dashboard') }}">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>

        <li class="nav-item nav-title mt-3">
            Master Data
        </li>

        <li class="nav-item {{ activeClass('admin.jenis-autis.*') }}">
            <a class="nav-link" href="{{ route('admin.jenis-autis.index') }}">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Jenis Autis</span>
            </a>
        </li>

        <li class="nav-item {{ activeClass('admin.gejala.*') }}">
            <a class="nav-link" href="{{ route('admin.gejala.index') }}">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Gejala</span>
            </a>
        </li>

        <li class="nav-item {{ activeClass('admin.kriteria.*') }}">
            <a class="nav-link" href="{{ route('admin.kriteria.index') }}">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Kriteria</span>
            </a>
        </li>

        <li class="nav-item {{ activeClass('admin.lokasi-pengobatan.*') }}">
            <a class="nav-link" href="{{ route('admin.lokasi-pengobatan.index') }}">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Lokasi Pengobatan</span>
            </a>
        </li>

        <li class="nav-item nav-title mt-3">
            Uji Perhitungan
        </li>

        <li class="nav-item {{ activeClass('admin.perhitungan.*') }}">
            <a class="nav-link" href="{{ route('admin.perhitungan.pilih-gejala') }}">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Perhitungan</span>
            </a>
        </li>

        <li class="nav-item nav-title mt-3">
            User
        </li>

        <li class="nav-item {{ activeClass('admin.diagnosa.*') }}">
            <a class="nav-link" href="{{ route('admin.diagnosa.index') }}">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Diagnosa</span>
            </a>
        </li>
    </ul>
</nav>
