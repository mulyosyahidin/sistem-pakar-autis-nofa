<!-- =========== Navbar Start =========== -->
<header
    class="@@link-color fixed top-0 inset-x-0 flex items-center z-40 w-full bg-white transition-all py-3.5">
    <div class="container">
        <nav class="flex items-center">
            <!-- Navbar Brand Logo -->
            <a href="{{ route('home') }}">
                <img src="{{ asset('assets/images/logo-dark.png') }}" class="h-8 logo-dark" alt="Logo Dark">
                <img src="{{ asset('assets/images/logo.png') }}" class="h-8 logo-light"
                    alt="Logo Light">
            </a>

            <!-- Nevigation Menu -->
            <div class="hidden lg:block mx-auto grow">
                <ul id="navbar-navlist"
                    class="grow flex flex-col lg:flex-row lg:items-center lg:justify-center mt-4 lg:mt-0">
                    <li class="nav-item pe-4">
                        <a class="nav-link flex items-center font-medium py-2 px-4 lg:py-0 {{ activeClass('user.dashboard', 'text-primary') }} hover:text-primary"
                            href="{{ route('user.dashboard') }}">
                            <span class="shrink-0 me-2">
                                <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <title>Stockholm-icons / Layout / Layout-4-blocks</title>
                                    <desc>Created with Sketch.</desc>
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                        <rect id="Rectangle-7" fill="currentColor" x="4" y="4" width="7"
                                            height="7" rx="1.5"></rect>
                                        <path
                                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                            id="Combined-Shape" fill="currentColor" opacity="0.3"></path>
                                    </g>
                                </svg>
                            </span>
                            <span class="grow">Dashboard</span>
                        </a>
                    </li>

                    <li class="nav-item pe-4">
                        <a class="nav-link flex items-center font-medium py-2 px-4 lg:py-0 {{ activeClass('user.diagnosa.*', 'text-primary') }} hover:text-primary"
                            href="{{ route('user.diagnosa.index') }}">
                            <span class="shrink-0 me-2">
                                <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <title>Stockholm-icons / Text / Article</title>
                                    <desc>Created with Sketch.</desc>
                                    <g id="Stockholm-icons-/-Text-/-Article" stroke="none" stroke-width="1"
                                        fill="none" fill-rule="evenodd">
                                        <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                        <rect id="Rectangle-20" fill="currentColor" x="4" y="5" width="16"
                                            height="3" rx="1.5"></rect>
                                        <path
                                            d="M5.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L5.5,18 C4.67157288,18 4,17.3284271 4,16.5 C4,15.6715729 4.67157288,15 5.5,15 Z M5.5,10 L12.5,10 C13.3284271,10 14,10.6715729 14,11.5 C14,12.3284271 13.3284271,13 12.5,13 L5.5,13 C4.67157288,13 4,12.3284271 4,11.5 C4,10.6715729 4.67157288,10 5.5,10 Z"
                                            id="Combined-Shape" fill="currentColor" opacity="0.3"></path>
                                    </g>
                                </svg>
                            </span>
                            <span class="grow">Riwayat Diagnosa</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="block grow ms-auto lg:shrink me-4 lg:me-0">
                <ul class="navbar-nav flex gap-x-3 items-center justify-end lg:justify-center">
                    <!-- Inner Page Dropdown -->
                    <li class="nav-item">
                        <a href="javascript:void(0);"
                            class="nav-link after:absolute hover:after:-bottom-10 after:inset-0"
                            data-fc-target="innerPageDropdownMenu" data-fc-type="dropdown" data-fc-placement="bottom">
                            <div class="flex items-center">
                                <div class="shrink">
                                    <div class="h-8 w-8 me-2">
                                        <img src="{{ auth()->user()->getProfilePicture() }}"
                                            class="avatar h-full w-full rounded-full me-2" alt="">
                                    </div>
                                </div>
                                <div class="hidden lg:block grow ms-1 leading-normal">
                                    <span class="block text-sm font-medium">{{ auth()->user()->name }}</span>
                                    <span class="block text-gray-400 text-xs">User</span>
                                </div>
                            </div>
                        </a>

                        <div id="innerPageDropdownMenu"
                            class="hidden opacity-0 mt-4 fc-dropdown-open:opacity-100 fc-dropdown-open:translate-y-0 translate-y-3 origin-center transition-all bg-white rounded-lg shadow-lg border p-2 w-48 space-y-1.5">
                            <!-- Dropdown item -->
                            <div class="nav-item rounded hover:bg-slate-100 transition-all">
                                <a class="nav-link !p-2" href="{{ route('user.profile.index') }}">
                                    <svg class="h-4 w-4 me-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-user icon icon-xxs me-1 icon-dual">
                                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="12" cy="7" r="4"></circle>
                                    </svg>
                                    Profile
                                </a>
                            </div>

                            <hr class="-mx-2 my-2">

                            <!-- Dropdown item -->
                            <div class="nav-item rounded hover:bg-slate-100 transition-all">
                                <a class="nav-link !p-2 logout-link" href="#">
                                    <svg class="h-4 w-4 me-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-unlock icon icon-xxs me-1 icon-dual">
                                        <rect x="3" y="11" width="18" height="11" rx="2"
                                            ry="2"></rect>
                                        <path d="M7 11V7a5 5 0 0 1 9.9-1"></path>
                                    </svg>
                                    Sign Out
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <!-- Moblie Menu Toggle Button (Offcanvas Button) -->
            <div class="lg:hidden flex items-center ms-auto px-2.5">
                <button type="button" data-fc-target="mobileMenu" data-fc-type="offcanvas">
                    <i class="fa-solid fa-bars text-2xl text-gray-500"></i>
                </button>
            </div>
        </nav>
    </div>
</header>
<!-- =========== Navbar End =========== -->

<!-- =========== Mobile Menu Start (Offcanvas) =========== -->
<div id="mobileMenu"
    class="fc-offcanvas-open:-translate-x-0 -translate-x-full fixed top-0 end-0 transition-all duration-200 transform h-full w-full max-w-md z-50 bg-white border-e hidden">
    <div class="flex flex-col h-full divide-y-2 divide-gray-200">
        <!-- Mobile Menu Topbar Logo (Header) -->
        <div class="p-6 flex items-center justify-between">
            <a href="{{ route('home') }}">
                <img src="{{ asset('assets/themes/prompt/images/logo-dark.png') }}" class="h-8" alt="Logo">
            </a>

            <button data-fc-dismiss class="flex items-center px-2">
                <i class="fa-solid fa-xmark text-xl"></i>
            </button>
        </div>

        <!-- Mobile Menu Link List -->
        <div class="p-6 overflow-scroll h-full">
            <ul class="navbar-nav flex flex-col gap-2" data-fc-type="accordion">
                <!-- Home Page Link -->
                <li class="nav-item">
                    <a href="{{ route('user.dashboard') }}" class="nav-link">
                        <span class="shrink-0 me-2">
                            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>Stockholm-icons / Layout / Layout-4-blocks</title>
                                <desc>Created with Sketch.</desc>
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                    <rect id="Rectangle-7" fill="currentColor" x="4" y="4" width="7"
                                        height="7" rx="1.5"></rect>
                                    <path
                                        d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                        id="Combined-Shape" fill="currentColor" opacity="0.3"></path>
                                </g>
                            </svg>
                        </span>
                        <span class="grow">Dashboard</span>
                    </a>
                </li>

                <!-- Home Page Link -->
                <li class="nav-item">
                    <a href="{{ route('user.diagnosa.index') }}" class="nav-link">
                        <span class="shrink-0 me-2">
                            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>Stockholm-icons / Text / Article</title>
                                <desc>Created with Sketch.</desc>
                                <g id="Stockholm-icons-/-Text-/-Article" stroke="none" stroke-width="1"
                                    fill="none" fill-rule="evenodd">
                                    <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                                    <rect id="Rectangle-20" fill="currentColor" x="4" y="5" width="16"
                                        height="3" rx="1.5"></rect>
                                    <path
                                        d="M5.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L5.5,18 C4.67157288,18 4,17.3284271 4,16.5 C4,15.6715729 4.67157288,15 5.5,15 Z M5.5,10 L12.5,10 C13.3284271,10 14,10.6715729 14,11.5 C14,12.3284271 13.3284271,13 12.5,13 L5.5,13 C4.67157288,13 4,12.3284271 4,11.5 C4,10.6715729 4.67157288,10 5.5,10 Z"
                                        id="Combined-Shape" fill="currentColor" opacity="0.3"></path>
                                </g>
                            </svg>
                        </span>
                        <span class="grow">Riwayat Diagnosa</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- =========== Mobile Menu End =========== -->
