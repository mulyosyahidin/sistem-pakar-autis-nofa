<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>@yield('title') - {{ config('app.name', 'Laravel') }}</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/vendors/feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/js/select.dataTables.min.css') }}">
    <!-- endinject -->

    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/css/vertical-layout-light/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/simplelineicons/css/simple-line-icons.css') }}">
    <!-- endinject -->

    <link rel="shortcut icon" href="{{ asset('assets/themes/skydash/images/logo-mini.svg') }}" />

    @yield('custom_head')

    <link rel="stylesheet"
        href="{{ asset('assets/themes/skydash/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/themes/skydash/vendors/select2/select2.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/themes/skydash/vendors/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/toastify-js/src/toastify.css') }}">

    <style>
        .btn-warning,
        .btn-warning:hover {
            color: white;
        }

        .btn-30 {
            height: 30px;
        }

        .nav-title {
            padding: 0 0.8125rem;
            color: #6C7383;
            font-weight: bold;
            text-transform: uppercase;
        }

        .table-border-except {
            border-top: none !important;
        }

        .table-border-except th:first-child,
        .table-border-except td:first-child {
            border-left: none !important;
        }

        .table-border-except th:last-child,
        .table-border-except td:last-child {
            border-right: none !important;
        }

        .table-border-except tr:last-child td {
            border-bottom: none !important;
        }

        .no-border-top tr:first-child td {
            border-top: none !important;
        }
    </style>
</head>

<body>
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        @include('layouts.includes.admin.topbar')
        <!-- partial -->

        <div class="container-fluid page-body-wrapper">
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            @include('layouts.includes.admin.sidebar')
            <!-- partial -->
            <div class="main-panel">
                @yield('content')

                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                @include('layouts.includes.admin.footer')
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    @yield('custom_html')

    <form action="{{ route('logout') }}" method="post" id="logout-form">
        @csrf
    </form>

    <!-- plugins:js -->
    <script src="{{ asset('assets/themes/skydash/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->

    <!-- inject:js -->
    <script src="{{ asset('assets/themes/skydash/js/off-canvas.js') }}"></script>
    <script src="{{ asset('assets/themes/skydash/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('assets/themes/skydash/js/template.js') }}"></script>
    <script src="{{ asset('assets/themes/skydash/js/settings.js') }}"></script>
    <script src="{{ asset('assets/themes/skydash/js/todolist.js') }}"></script>

    <script src="{{ asset('assets/themes/skydash/vendors/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/themes/skydash/vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/toastify-js/src/toastify.js') }}"></script>

    <script src="{{ asset('assets/themes/skydash/vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/themes/skydash/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <!-- endinject -->

    @stack('custom_js')

    <script>
        let logoutLink = document.querySelector('.logout-link');

        logoutLink.addEventListener('click', function(e) {
            e.preventDefault();

            document.getElementById('logout-form').submit();
        });
    </script>

    @if (session()->has('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Berhasil!',
                text: `{{ session()->get('success') }}`,
            })
        </script>
    @endif

    @if (session()->has('error'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Terjadi kesalahan!',
                text: `{{ session()->get('error') }}`,
            })
        </script>
    @endif

    <script>
        (function($) {
            'use strict';
            $(function() {
                $('.datatable').DataTable({
                    "aLengthMenu": [
                        [5, 10, 15, 50, 100, -1],
                        [5, 10, 15, 50, 100, "Semua"]
                    ],
                    "iDisplayLength": 10,
                    "language": {
                        search: ""
                    }
                });
                $('.datatable').each(function() {
                    var datatable = $(this);
                    // SEARCH - Add the placeholder for Search and Turn this into in-line form control
                    var search_input = datatable.closest('.dataTables_wrapper').find(
                        'div[id$=_filter] input');
                    search_input.attr('placeholder', 'Search');
                    search_input.removeClass('form-control-sm');
                    // LENGTH - Inline-Form control
                    var length_sel = datatable.closest('.dataTables_wrapper').find(
                        'div[id$=_length] select');
                    length_sel.removeClass('form-control-sm');
                });
            });
        })(jQuery);
    </script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                theme: 'bootstrap-5',
            });
        });
    </script>
</body>

</html>
