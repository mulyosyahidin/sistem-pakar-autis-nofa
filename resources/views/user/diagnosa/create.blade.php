@extends('layouts.user')
@section('title', 'Diagnosa Baru')

@section('content')
    <div class="bg-slate-100 h-full mt-[77px]  py-3 px-3">
        <section class="relative overflow-hidden">
            <div class="container">
                <div class="flex">
                    <div class="w-full">
                        <h3 class="text-xl text-gray-800 mt-2">Diagnosa Baru</h3>
                        <p class="mt-1 font-medium mb-4">Pilih intensitas setiap gejala untuk melakukan diagnosa</p>
                    </div>
                </div>

                <div class="flex mt-2">
                    <div class="w-full">
                        <div class="bg-white rounded">
                            <div class="p-6">
                                <div class="lg:col-span-3 transition-all px-4 h-full">
                                    <div id="account" class="min-h-[650px]">
                                        <form action="{{ route('user.diagnosa.store') }}" method="POST">
                                            @csrf

                                            <div class=" mt-6 mb-6">
                                                <div class="mb-4">
                                                    <label for="name"
                                                        class="block text-sm font-semibold mb-3 text-gray-600">Nama
                                                        Diagnosa</label>
                                                    <input type="text"
                                                        class="py-2 px-4 leading-6 block w-full text-gray-700 border-gray-300 rounded text-sm focus:border-gray-300 focus:ring-0"
                                                        id="name" name="nama" value="{{ old('nama') }}" required>
                                                </div>
                                            </div>
                                            <!-- basic info end -->

                                            <h4 class="text-base text-gray-800">Intensitas Gejala</h4>
                                            <hr class="mb-3 mt-2">

                                            <div class="relative overflow-x-auto">
                                                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                                                    <thead
                                                        class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                                        <tr>
                                                            <th scope="col" class="px-6 py-3">
                                                                Gejala
                                                            </th>
                                                            <th scope="col" class="px-6 py-3">
                                                                Kode
                                                            </th>
                                                            <th scope="col" class="px-6 py-3">
                                                                Intensitas
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($gejala as $item)
                                                            <tr
                                                                class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                                                <td class="px-6 py-4">
                                                                    {{ $item->gejala }}
                                                                </td>
                                                                <td class="px-6 py-4">
                                                                    {{ $item->kode }}
                                                                </td>
                                                                <td class="px-6 py-4">
                                                                    <select name="nilai[{{ $item->id }}]"
                                                                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                                        required>
                                                                        <option selected disabled>Pilih Intensitas</option>
                                                                        @foreach ($kriteria as $dataKriteria)
                                                                            <option value="{{ $dataKriteria->id }}">
                                                                                {{ $dataKriteria->keterangan }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>

                                            <!-- save start -->
                                            <div class="flex mt-6">
                                                <div class="w-full flex justify-end">
                                                    <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">
                                                        Lakukan Diagnosa
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- save end -->
                                        </form>
                                    </div><!-- end tab -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
