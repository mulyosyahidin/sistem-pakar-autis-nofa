@extends('layouts.user')
@section('title', 'Diagnosa ' . $diagnosa->nama)

@section('custom_head')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
        integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin="" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@3.2.12/dist/leaflet-routing-machine.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.css" />

    <style>
        #map {
            height: 600px;
            border-radius: 8px;
        }
    </style>
@endsection

@section('content')
    <div class="bg-slate-100 h-full mt-[77px]  py-3 px-3">
        <section class="relative overflow-hidden">
            <div class="container">
                <div class="flex">
                    <div class="w-full">
                        <h3 class="text-xl text-gray-800 mt-2">Data Diagnosa</h3>
                    </div>
                </div>

                <div class="flex mt-2">
                    <div class="w-full">
                        <div class="bg-white rounded">
                            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                                <tbody>
                                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                        <td class="px-6 py-4">
                                            <strong>Judul</strong>
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ $diagnosa->judul }}
                                        </td>
                                    </tr>
                                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                        <td class="px-6 py-4">
                                            <strong>Tanggal</strong>
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ $diagnosa->created_at->translatedFormat('l, d M Y H:i') }}
                                        </td>
                                    </tr>
                                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                        <td class="px-6 py-4">
                                            <strong>Hasil Diagnosa</strong>
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ $diagnosa->jenisAutis->nama }}
                                        </td>
                                    </tr>
                                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                        <td class="px-6 py-4">
                                            <strong>Nilai</strong>
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ $diagnosa->nilai }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card mt-3">
                    <div id="map"></div>
                </div>

                <div class="lokasi-terdekat">
                    <div class="flex mt-5 mb-3">
                        <div class="w-full">
                            <p class="mt-1 font-medium hide-this">
                                Lokasi Pengobatan Terdekat
                            </p>

                            <p class="default-message hide-this">
                                <small>Mohon izinkan browser untuk mengakses lokasi Anda</small>
                            </p>

                            <div class="error-message flex items-center p-4 mb-4 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
                                role="alert" style="display: none;">
                                <svg class="flex-shrink-0 inline w-4 h-4 mr-3" aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
                                    <path
                                        d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM9.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM12 15H8a1 1 0 0 1 0-2h1v-3H8a1 1 0 0 1 0-2h2a1 1 0 0 1 1 1v4h1a1 1 0 0 1 0 2Z" />
                                </svg>
                                <span class="sr-only">Info</span>
                                <div>
                                    Gagal mendapatkan data lokasi. Mohon izinkan browser untuk mengakses lokasi Anda.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex hide-this">
                    <div class="w-full">
                        <div class="bg-white rounded">
                            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                                <tbody>
                                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                        <td class="px-6 py-4">
                                            <strong>Nama</strong>
                                        </td>
                                        <td class="px-6 py-4">
                                            <span class="nama-lokasi">...</span>
                                        </td>
                                    </tr>
                                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                        <td class="px-6 py-4">
                                            <strong>Alamat</strong>
                                        </td>
                                        <td class="px-6 py-4">
                                            <span class="alamat-lokasi">...</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="flex mt-5">
                    <div class="w-full">
                        <p class="mt-1 font-medium mb-4">
                            Data Intensitas Gejala
                        </p>
                    </div>
                </div>

                <div class="flex mt-2">
                    <div class="w-full">
                        <div class="bg-white rounded">
                            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                                <thead
                                    class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                    <tr>
                                        <th scope="col" class="px-6 py-3">
                                            Kode
                                        </th>
                                        <th scope="col" class="px-6 py-3">
                                            Gejala
                                        </th>
                                        <th scope="col" class="px-6 py-3">
                                            Intensitas
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($diagnosa->gejala as $gejala)
                                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                            <td class="px-6 py-4">
                                                {{ $gejala->kode }}
                                            </td>
                                            <td class="px-6 py-4">
                                                {{ $gejala->gejala }}
                                            </td>
                                            <td class="px-6 py-4">
                                                {{ $gejala->pivot->kriteria->keterangan }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('custom_js')
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
        integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>
    <script src="https://unpkg.com/leaflet-routing-machine@3.2.12/dist/leaflet-routing-machine.js"></script>
    <script src="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.js"></script>

    <script>
        let map = document.querySelector('#map');
        let hideThis = document.querySelectorAll('.hide-this');
        let errorMessage = document.querySelector('.error-message');

        let lokasiPengobatan = @json($lokasiPengobatan);
        let namaLokasiTerdekat = document.querySelector('span.nama-lokasi');
        let alamatLokasiTerdekat = document.querySelector('span.alamat-lokasi');
        let defaultMessage = document.querySelector('.default-message');

        if ("geolocation" in navigator) {
            // Browser mendukung Geolocation API
            navigator.geolocation.getCurrentPosition(function(position) {
                var userLatitude = position.coords.latitude;
                var userLongitude = position.coords.longitude;

                console.log("User Latitude: " + userLatitude);
                console.log("User Longitude: " + userLongitude);

                // Hitung jarak antara pengguna dan setiap lokasi pengobatan
                lokasiPengobatan.forEach(function(lokasi) {
                    var radius = 6371; // Radius bumi dalam kilometer
                    var dLat = (lokasi.latitude - userLatitude) * Math.PI / 180;
                    var dLon = (lokasi.longitude - userLongitude) * Math.PI / 180;
                    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(userLatitude * Math.PI /
                            180) * Math.cos(lokasi.latitude * Math.PI / 180) * Math.sin(dLon / 2) * Math
                        .sin(
                            dLon / 2);
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    var distance = radius * c;

                    lokasi.distance = distance; // Tambahkan jarak ke setiap lokasi pengobatan
                });

                // Urutkan lokasi pengobatan berdasarkan jarak
                lokasiPengobatan.sort(function(a, b) {
                    return a.distance - b.distance;
                });

                // Pilih lokasi pengobatan terdekat
                var lokasiTerdekat = lokasiPengobatan[0];

                console.log("Lokasi pengobatan terdekat:");
                console.log(lokasiTerdekat);

                namaLokasiTerdekat.innerHTML = lokasiTerdekat.nama;
                alamatLokasiTerdekat.innerHTML = lokasiTerdekat.alamat;

                defaultMessage.innerHTML = '';

                var map = L.map('map').setView([
                    userLatitude, userLongitude
                ], 14);

                // var marker = L.marker([userLatitude, userLongitude]).addTo(map);

                //add radius to marker
                var circle = L.circle([userLatitude, userLongitude], {
                    color: 'blue',
                    fillColor: '#90cdf4',
                    fillOpacity: 0.2,
                    radius: 100,
                }).addTo(map);

                //add popup to marker radius
                circle.bindPopup("Perkiraan lokasi saya");

                var greenIcon = new L.Icon({
                    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
                    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                    iconSize: [25, 41],
                    iconAnchor: [12, 41],
                    popupAnchor: [1, -34],
                    shadowSize: [41, 41]
                });

                lokasiPengobatan.forEach(function(lokasi) {
                    if (lokasi.id != lokasiTerdekat.id) {
                        var lokasiMarker = L.marker([lokasi.latitude, lokasi.longitude]).addTo(map);
                        lokasiMarker.bindPopup(lokasi.nama);
                    }
                });

                map.attributionControl.setPrefix('');

                // Menambahkan jalur routing dengan menggunakan Layanan Routing OpenStreetMap (OSRM)
                var routingControl = L.Routing.control({
                    waypoints: [
                        L.latLng(userLatitude, userLongitude),
                        L.latLng(lokasiTerdekat.latitude, lokasiTerdekat.longitude)
                    ],
                    routeWhileDragging: true, // Menghitung rute saat menggeser marker
                    geocoder: L.Control.Geocoder.nominatim(),
                }).addTo(map);

                routingControl.on('routesfound', function(e) {
                    var routes = e.routes;
                    var summary = routes[0].summary;

                    console.log('Jarak: ' + summary.totalDistance + ' meter');
                    console.log('Waktu tempuh: ' + summary.totalTime / 3600 + ' jam');
                });

                L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 19,
                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                }).addTo(map);
            }, function(error) {
                // Tangani kesalahan jika pengguna menolak atau terjadi kesalahan lainnya
                console.error("Gagal mendapatkan lokasi: " + error.message);

                errorMessage.style.display = 'block';
                map.style.display = 'none';
                hideThis.forEach(function(element) {
                    element.style.display = 'none';
                });

                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi kesalahan!',
                    text: 'Gagal mendapatkan lokasi Anda. Mohon izinkan browser untuk mengakses lokasi Anda.'
                });
            });
        } else {
            // Browser tidak mendukung Geolocation API
            console.log("Geolocation tidak didukung di browser ini.");

            Swal.fire({
                icon: 'error',
                title: 'Terjadi kesalahan!',
                text: 'Browser yang Anda gunakan tidak mendukung GeoLocation. Silahkan gunakan browser lain untuk mendapatkan petunjuk arah menuju lokasi pengobatan terdekat.'
            });
        }
    </script>
@endpush
