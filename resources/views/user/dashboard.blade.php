@extends('layouts.user')
@section('title', 'User Dashboard')

@section('content')
    <div class="bg-slate-100  mt-[77px]  py-3 px-3">
        <section class="relative overflow-hidden">
            <div class="container">
                <div class="flex">
                    <div class="w-full">
                        <h3 class="text-xl text-gray-800 mt-2">Hi {{ auth()->user()->name }}</h3>
                        <p class="mt-1 font-medium mb-4">Selamat Datang!</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="relative overflow-hidden">
            <div class="container">
                <div class="flex items-center justify-between my-6">
                    <div class="">
                        <h4 class="text-base text-gray-800">Diagnosa Terbaru</h4>
                    </div>
                    <div class="text-end">
                        <a href="{{ route('user.diagnosa.create') }}" class="font-semibold text-primary text-sm">Diagnosa
                            Baru
                        </a>
                        &bull;
                        <a href="{{ route('user.diagnosa.index') }}" class="font-semibold text-primary text-sm">Lihat Semua
                        </a>
                    </div>
                </div><!-- end title -->

                <div class="flex flex-col gap-y-2 w-full mb-4">
                    @foreach ($riwayat as $item)
                        <div class="bg-white">
                            <div class="p-6">
                                <div class="flex flex-wrap lg:flex-nowrap items-center sm:justify-between gap-y-2">
                                    <div class="w-1/2">
                                        <div class="flex items-center gap-2">
                                            <label class="text-sm font-semibold text-gray-700" for="task1">
                                                {{ $item->judul }}
                                            </label>
                                        </div> <!-- end checkbox -->
                                    </div> <!-- end col -->
                                    <div class="lg:w-1/3">
                                        <span
                                            class="inline-flex items-center gap-1.5 py-0.5 px-2 rounded-full text-sm font-semibold bg-blue-400/10 text-cyan-500">
                                            {{ $item->jenisAutis->nama }}
                                        </span>
                                    </div>
                                    <div class="lg:w-2/3 text-right">
                                        <a href="{{ route('user.diagnosa.show', $item) }}"
                                            class="py-0.5 px-2 rounded-full text-blue-500 bg-blue-400 hover:bg-blue-200 text-white">
                                            <strong>&RightArrow;</strong>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
@endsection
