@extends('layouts.user')
@section('title', 'Profile Saya')

@section('content')
    <div class="bg-slate-100 h-full mt-[77px]  py-3 px-3">
        <section class="relative overflow-hidden">
            <div class="container">
                <div class="flex">
                    <div class="w-full">
                        <h3 class="text-xl text-gray-800 mt-2">Profil Saya</h3>
                    </div>
                </div>

                <div class="flex mt-2">
                    <div class="w-full">
                        <div class="bg-white rounded">
                            <div class="p-6">
                                <div class="lg:col-span-3 transition-all px-4 h-full">
                                    <form action="{{ route('user.profile.update') }}" method="POST"
                                        enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')

                                        <!-- avatar start -->
                                        <h6 class="mt-6 mb-2.5 text-sm text-gray-800">Foto Profil</h6>
                                        <div class="flex items-center gap-6">
                                            <div class="sharink">
                                                <div class="w-16 h-16">
                                                    <img src="{{ auth()->user()->getProfilePicture() }}"
                                                        class="max-w-full max-h-full rounded-full shadow" alt="...">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- avatar end -->

                                        <div class=" mt-6 mb-6">
                                            <div class="mb-4">
                                                <label for="file"
                                                    class="block text-sm font-semibold mb-3 text-gray-600">Foto Profil
                                                </label>
                                                <input type="file"
                                                    class="py-2 px-4 leading-6 block w-full text-gray-700 border-gray-300 rounded text-sm focus:border-gray-300 focus:ring-0"
                                                    id="file" name="picture">
                                            </div>
                                        </div>

                                        <hr class="my-6">

                                        <div class=" mt-6 mb-6">
                                            <div class="mb-4">
                                                <label for="name"
                                                    class="block text-sm font-semibold mb-3 text-gray-600">Nama
                                                </label>
                                                <input type="text"
                                                    class="py-2 px-4 leading-6 block w-full text-gray-700 border-gray-300 rounded text-sm focus:border-gray-300 focus:ring-0"
                                                    id="name" name="name"
                                                    value="{{ old('name', auth()->user()->name) }}" required>
                                            </div>
                                        </div>

                                        <div class="grid grid-cols-2 gap-6">
                                            <div class="mb-4">
                                                <label for="email"
                                                    class="block text-sm font-semibold mb-3 text-gray-600">Email</label>
                                                <input type="text"
                                                    class="py-2 px-4 leading-6 block w-full text-gray-700 border-gray-300 rounded text-sm focus:border-gray-300 focus:ring-0"
                                                    id="email" name="email"
                                                    value="{{ old('email', auth()->user()->email) }}" required>
                                            </div>
                                            <div class="mb-4">
                                                <label for="password"
                                                    class="block text-sm font-semibold mb-3 text-gray-600">Password</label>
                                                <input type="password"
                                                    class="py-2 px-4 leading-6 block w-full text-gray-700 border-gray-300 rounded text-sm focus:border-gray-300 focus:ring-0"
                                                    id="password" name="password">
                                            </div>
                                        </div>

                                        <hr class="my-4">

                                        <!-- save start -->
                                        <div class=" mt-3 flex justify-end">
                                            <button type="submit"
                                                class="inline-flex items-center justify-center py-3 px-4 rounded-lg text-sm font-semibold transition-all hover:shadow-lg bg-primary text-white hover:shadow-primary/30 focus:shadow-none focus:outline focus:outline-primary/40 ">
                                                Simpan
                                            </button>
                                        </div>
                                        <!-- save end -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
